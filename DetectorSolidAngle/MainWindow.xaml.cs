﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MathNet.Numerics.Integration;
namespace DetectorSolidAngle
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private double PaddleLength, PaddleWidth, PaddleThickness, PaddleSeparation;
        private int _angle;
        public event PropertyChangedEventHandler PropertyChanged;
        private double Omega_RE;
        public int theta
        {
            get
            {
                return _angle;
            }
            set
            {
                if (_angle != value)
                {
                    _angle = value;
                    OnPropertyChanged("theta");
                }
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            scrollbar.Value = 0;
            this.angleval.Text = ((int)scrollbar.Value).ToString();
        }

        private void scrollbar_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {
            this.angleval.Text = ((int)scrollbar.Value).ToString();
            theta = -1 * (int)scrollbar.Value;
            RotateTransform rot = new RotateTransform(theta, (double)100,(double)40);
            paddles.RenderTransform = rot;
        }

       

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void CalculateOmega_Click(object sender, RoutedEventArgs e)
        {
            if (PaddleSeparation != 0 && PaddleThickness != 0 && PaddleWidth != 0)
            {
                double u,v;
                //double dOmega = PaddleSeparation/Math.Pow(Math.Pow(PaddleSeparation,2)+Math.Pow(u,2)+Math.Pow(v,2),1.5); //(1) / (Math.Pow(Math.Sin(theta*Math.PI/180.0),2));
                if (theta != 0)
                {
                    Omega_RE = Math.Asin(Math.Sqrt(Math.Pow(PaddleWidth, 2) * Math.Pow(PaddleLength, 2) / (Math.Pow(PaddleSeparation, 2) + Math.Pow(PaddleWidth, 2)) * (Math.Pow(PaddleSeparation, 2) + Math.Pow(PaddleLength, 2)))) / Math.Pow(Math.Sin(theta * Math.PI / 180.0), 2);
                }
                else Omega_RE = Omega_RE = Math.Asin(Math.Sqrt(Math.Pow(PaddleWidth, 2) * Math.Pow(PaddleLength, 2) / (Math.Pow(PaddleSeparation, 2) + Math.Pow(PaddleWidth, 2)) * (Math.Pow(PaddleSeparation, 2) + Math.Pow(PaddleLength, 2))));

                OmegaTB.Text = Omega_RE.ToString();
            }
            else MessageBox.Show("Żaden z wymiarów detektora nie może być równy 0!\nSzerokość: "+PaddleWidth.ToString()+"\n Długość: "+PaddleLength.ToString()+"\n Separacja: "+PaddleSeparation.ToString(), "Błąd wprowadzania danych", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        #region Sprawdzanie poprawności wprowadzanych danych liczbowych
        private void PaddleSeparationTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            double i;
            if (!double.TryParse(PaddleSeparationTB.Text, out i))
            {
                MessageBox.Show("Podana wartość nie jest liczbą!","Błąd wprowadzania danych",MessageBoxButton.OK,MessageBoxImage.Error);
                PaddleSeparationTB.Focus();
                PaddleSeparationTB.Background = Brushes.Red;
                return;
            }
            else 
            {
                PaddleSeparationTB.Background = Brushes.Transparent;
                PaddleSeparation = i;
            }
            var tmpmargin = BottomPaddle.Margin.Bottom;
            //BottomPaddle.Margin.Bottom= tmpmargin+ 10 * Convert.ToDouble(PaddleSeparationTB.Text);
        }

        private void PaddleThicknessTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            double i;
            if (!double.TryParse(PaddleThicknessTB.Text, out i))
            {
                MessageBox.Show("Podana wartość nie jest liczbą!", "Błąd wprowadzania danych", MessageBoxButton.OK, MessageBoxImage.Error);
                PaddleThicknessTB.Focus();
                PaddleThicknessTB.Background = Brushes.Red;
                return;
            }
            else
            {
                PaddleThicknessTB.Background = Brushes.Transparent;
                PaddleThickness = i;
            }
        }

        private void PaddleLengthTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            double i;
            if (!double.TryParse(PaddleLengthTB.Text, out i))
            {
                MessageBox.Show("Podana wartość nie jest liczbą!", "Błąd wprowadzania danych", MessageBoxButton.OK, MessageBoxImage.Error);
            PaddleLengthTB.Focus();
                PaddleLengthTB.Background = Brushes.Red;
                return;
            }
            else
            {
                PaddleLengthTB.Background = Brushes.Transparent;
                PaddleLength = i;
            }
        }

        private void PaddleWidthTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            double i;
            if (!double.TryParse(PaddleWidthTB.Text, out i))
            {
                MessageBox.Show("Podana wartość nie jest liczbą!", "Błąd wprowadzania danych", MessageBoxButton.OK, MessageBoxImage.Error);
                PaddleWidthTB.Focus();
                PaddleWidthTB.Background = Brushes.Red;
                return;
            }
            else
            {
                PaddleWidthTB.Background = Brushes.Transparent;
                PaddleWidth = i;
            }
        }

        #endregion
    }
}
